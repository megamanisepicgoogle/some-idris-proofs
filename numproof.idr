plus_commutes_Z : m = plus m 0
plus_commutes_Z {m = Z} = Refl
plus_commutes_Z {m = (S k)} = let rec = plus_commutes_Z {m=k} in
                                  rewrite rec in Refl

plus_commutes_S : (k : Nat) -> (m : Nat) -> S (plus m k) = plus m (S k)
plus_commutes_S k Z = Refl
plus_commutes_S k (S j) = rewrite plus_commutes_S k j in Refl


plus_commutes : (n : Nat) -> (m : Nat) -> n + m = m + n
plus_commutes Z m = plus_commutes_Z
plus_commutes (S k) m = rewrite plus_commutes k m in (plus_commutes_S k m)

eq_commutes : (x : a) -> (y : b) -> x = y -> y = x
eq_commutes y y Refl = Refl

eq_trans : (x : a) -> (y : b) -> (z : c) -> x = y -> y = z -> x = z
eq_trans y y y Refl Refl = Refl

equal_succ_S : S (S k) = S (S k)
equal_succ_S = Refl

equal_succ :  {x : Nat} -> {y : Nat} -> x = y -> (S x) = (S y)
equal_succ prf = rewrite prf in Refl

plus_assoc : (a : Nat) -> (b : Nat) -> (c : Nat) -> ((a+b)+c) = (a+(b+c))
plus_assoc_rhs2 : (k : Nat) -> (b : Nat) -> (c : Nat) -> S (plus (plus k b) c) = S (plus k (plus b c))

plus_assoc_rhs2 k b c = rewrite plus_assoc k b c in Refl
plus_assoc Z b c = Refl
plus_assoc (S k) b c = plus_assoc_rhs2 k b c

plus_const_l : (x : Nat) -> (y : Nat) -> (n : Nat) -> (x = y) -> plus n x = plus n y
plus_const_l x y Z prf = rewrite prf in Refl
plus_const_l x y (S k) prf = rewrite plus_const_l x y k prf in Refl

plus_0_id_rhs : (x : Nat) -> plus x Z = x
plus_0_id_rhs Z = Refl
plus_0_id_rhs (S k) = rewrite plus_0_id_rhs k in Refl

right_s_exchangeable_plus : (n : Nat) -> (m : Nat) -> plus n (S m) = S (plus n m)
right_s_exchangeable_plus Z m = Refl
right_s_exchangeable_plus (S k) m = rewrite right_s_exchangeable_plus k m in Refl

equal_f : (x : a) -> (y : a) -> (x = y) -> (f : a -> b) -> (f x) = (f y)
equal_f x y prf f = rewrite prf in Refl

lte_1_s : {n : Nat} -> LTE 1 (S n)
lte_1_s = LTESucc LTEZero

mult_id_l : {n : Nat} -> mult 1 n = n
mult_id_l {n} = rewrite plus_0_id_rhs n in Refl

mult_id_r : {n : Nat} -> mult n 1 = n
mult_id_r {n = Z}     = Refl
mult_id_r {n = (S k)} = rewrite mult_id_r {n = k} in Refl

power_id_r : {n : Nat} -> power n 1 = n
power_id_r {n = Z}     = Refl
power_id_r {n = (S k)} = rewrite mult_id_r {n = k} in Refl

mult_plus_2 : {n : Nat} -> plus n n = mult 2 n
mult_plus_2 {n} = rewrite plus_0_id_rhs n in Refl

mult_2_plus : {n : Nat} -> plus n n = mult n 2
mult_2_plus {n} = rewrite multCommutative n (S (S Z)) in mult_plus_2

plusminusid : {n : Nat} -> minus (plus (S Z) n) (S Z) = n
plusminusid {n = Z} = Refl
plusminusid {n = (S k)} = Refl

plusminusid' : {n : Nat} -> minus (plus n (S Z)) (S Z) = n
plusminusid' {n} = rewrite plus_commutes n (S Z) in plusminusid 

