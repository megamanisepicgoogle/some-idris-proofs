import Control.Isomorphism
import Data.Fin
import Data.Vect

%default total


interface Bifunctor (p : Type -> Type -> Type) where
  bimap : (a -> b) -> (c -> d) -> p a c -> p b d
  bimap f g = first f . second g

  first : (a -> b) -> p a c -> p b c
  first = flip bimap id

  second : (a -> b) -> p c a -> p c b
  second = bimap id

implementation Bifunctor Either where
  bimap f _ (Left  a) = Left  $ f a
  bimap _ g (Right b) = Right $ g b

implementation Bifunctor Pair where
  bimap f g (a, b) = (f a, g b)

data IsSucc : (a : Nat) -> (b : Nat) -> Type where
  SuccOf : {a : Nat} -> IsSucc a (S a)
  
data IsReflexive : {a : Type} -> (bin : a -> a -> Type) -> Type where
  Reflex : ({x : a} -> bin x x) -> IsReflexive bin

data IsSymmetric : {a : Type} -> (bin : a -> a -> Type) -> Type where
  Symm : ({x : a} -> {y : a} -> bin x y -> bin y x) -> IsSymmetric bin
  
data IsTransitive : {a : Type} -> (bin : a -> a -> Type) -> Type where
  Trans : ({x : a} -> {y : a} -> {z : a} -> bin x y -> bin y z -> bin x z) -> IsTransitive bin

data BinRel : (x : Type) -> (y : Type) -> Type where
  IsBin : (a -> b -> Type) -> BinRel a b

data IsAntiSymmetric : {a : Type} -> (bin : a -> a -> Type) -> Type where
  AntiSymm : ({x : a} -> {y : a} -> bin x y -> bin y x -> x = y) -> IsAntiSymmetric bin

data IsPartialOrder : {a : Type} -> (bin : a -> a -> Type) -> Type where
  Ispo : IsReflexive bin -> IsAntiSymmetric bin -> IsTransitive bin -> IsPartialOrder bin
  
data HasPartialOrder : (a : Type) -> Type where
  Poset : {x : Type} -> {bin : x -> x -> Type} -> (IsPartialOrder bin) -> HasPartialOrder x

data IsTotalOrder : {a : Type} -> (bin : a -> a -> Type) -> Type where
  TotalO : IsPartialOrder bin -> ((x : a) -> (y : a) -> Either (bin x y) (bin y x)) -> IsTotalOrder bin

data IsAssociative : {a : Type} -> (f : a -> a -> a) -> Type where
  Assoc : {x, y, z : a} -> ((x `f` y) `f` z) = (x `f` (y `f` z)) -> IsAssociative f

data N1 : Type where
  One : N1
  S1 : N1 -> N1

n1ton : N1 -> Nat
n1ton One = S Z
n1ton (S1 n) = S (n1ton n)

data MyInt : Type where
  ZI : MyInt
  Pos : N1 -> MyInt
  Neg : N1 -> MyInt


xneqsx : {n : Nat} -> n = S n -> Void
xneqsx Refl impossible

unitisntvoid : Iso () Void -> Void
unitisntvoid (MkIso to from toFrom fromTo) = to ()

--oneplusoneisosone : Iso (Either () ()) () -> Void
--oneplusoneisosone (MkIso to from toFrom fromTo) = ?oneplusoneisosone_rhs_1

data Has : (a : Type) -> (n : Nat) -> Type where
  MkCnt : Iso a (Fin n) -> Has a n
 

finonehasone : (y : Fin 1) -> const FZ (const () y) = y
finonehasone FZ = Refl
finonehasone (FS FZ) impossible
finonehasone (FS (FS _)) impossible

cucfzxisx : (x : ()) -> const () (const (Fin 1 `the` FZ) x) = x
cucfzxisx () = Refl

unitone : () `Has` 1
unitone = MkCnt (MkIso (const FZ) (const ()) finonehasone cucfzxisx)
 
b2f2 : Bool -> Fin 2
b2f2 False = FZ
b2f2 True = FS FZ
f22b : Fin 2 -> Bool
f22b FZ = False
f22b (FS FZ) = True

 
prf2 : (y : Fin 2) -> b2f2 (f22b y) = y
prf2 FZ = Refl
prf2 (FS FZ) = Refl
prf2 (FS (FS FZ)) impossible
prf2 (FS (FS (FS _))) impossible

prf3 : (x : Bool) -> f22b (b2f2 x) = x
prf3 False = Refl
prf3 True = Refl

booltwo : Bool `Has` 2
booltwo = MkCnt (MkIso b2f2 f22b prf2 prf3)

--data Pred : (x : Type) -> Type where
-- isPred : {b : Type} -> (b -> Type) -> Pred b
 
data TheTypeContaining : (a : Type) -> Type
    where Containing : (x : Type) -> TheTypeContaining x

neumannNat : Nat -> Type
neumannNat Z = Void
neumannNat (S n) = Either (neumannNat n) (TheTypeContaining (neumannNat n))

zermeloNat : Nat -> Type
zermeloNat Z = Void
zermeloNat (S k) = TheTypeContaining (zermeloNat k)

data Contains : (a : Type) -> (x : b) -> Type where
  Member : Contains y x {b = y}

lteqrefl : {x : Nat} -> LTE x x
lteqrefl {x = Z} = LTEZero
lteqrefl {x = (S k)} = LTESucc lteqrefl

lteqantisymm : LTE x2 y1 -> LTE y1 x2 -> x2 = y1
lteqantisymm LTEZero LTEZero = Refl
lteqantisymm (LTESucc x {left} {right}) (LTESucc y) = rewrite (lteqantisymm x y) in Refl


lteqtrans : LTE x3 y2 -> LTE y2 z1 -> LTE x3 z1
lteqtrans LTEZero LTEZero = LTEZero
lteqtrans LTEZero (LTESucc x) = LTEZero
lteqtrans (LTESucc x) (LTESucc y) = LTESucc (lteqtrans x y)

lteqispo : IsPartialOrder LTE
lteqispo = Ispo (Reflex lteqrefl) (AntiSymm lteqantisymm) (Trans lteqtrans)

natHasPo : HasPartialOrder Nat
natHasPo = Poset lteqispo

lteqforany : (x : Nat) -> (y : Nat) -> Either (LTE x y) (LTE y x)
lteqforany Z y = Left LTEZero
lteqforany (S k) Z = Right LTEZero
lteqforany (S k) (S j) = bimap LTESucc LTESucc $ lteqforany k j

lteqisto : IsTotalOrder LTE
lteqisto = TotalO lteqispo lteqforany

data HasFoundation : (bin : a -> a -> Type) -> (bot : a) -> Type where --what's the name?
  CreateBottom : ((x : a) -> bin bot x) -> HasFoundation bin bot

data HasUpperBound : (bin : a -> a -> Type) -> (top : a) -> Type where
   CreateUpper : ((x : a) -> bin x top) -> HasUpperBound bin top

lteqhasfound : LTE `HasFoundation` 0
lteqhasfound = CreateBottom (\x => LTEZero)

Powerset : Type -> Type
Powerset a = a -> Bool

Prop : Type
Prop = Type

Proof : (a : Type) -> Type
Proof a = a 

test : a -> Type
test x = Type

t : Type
t = test 0

infixl 5 :-
data Comb : Type where
  S : Comb
  K : Comb
  I : Comb
  Var : String -> Comb
  (:-) : Comb -> Comb -> Comb

Eq Comb where
  S == S = True
  K == K = True
  I == I = True
  (Var x) == (Var y) = x == y
  (x :- y) == (a :- b) = x == a && y == b
  _ == _ = False
  
infixr 5 /-
data BracketAbs : Type where
  ComB : Comb -> BracketAbs
  (/-) : String -> BracketAbs -> BracketAbs

isnf : Comb -> Bool
isnf (Var _) = True
isnf S = True
isnf K = True
isnf I = True
isnf (S:-_:-_:-__) = False
isnf (K:-_:-_) = False
isnf (I:-_) = False
isnf (x:-y) = isnf x && isnf y

reduce : Comb -> Comb
reduce (S:-x:-y:-z) = reduce (x :- z :- (y :- z))
reduce (K:-x:-y) = reduce x
reduce (I:-x) = reduce x
reduce (x:-y) = if isnf z then z else reduce z
  where z = ((reduce x):-(reduce y))
reduce x = x

freeVar : Comb -> List String
freeVar S = []
freeVar K = []
freeVar I = []
freeVar (Var x) = [x]
freeVar (x :- y) = freeVar x ++ freeVar y

bAbs : String -> Comb -> Comb
bAbs x S = K :- S
bAbs x K = K :- K
bAbs x I = K :- I
bAbs x (Var y) = if (Var x) == (Var y) then I else K :- (Var y)
bAbs x (y :- z) = if z == (Var x) && (not (x `elem` (freeVar y))) then y else S :- (bAbs x y) :- (bAbs x z)

{-
bracketAbs : BracketAbs -> Comb
bracketAbs (ComB x) = x
bracketAbs (x /- (ComB S)) = K :- S
bracketAbs (x /- (ComB K)) = K :- K
bracketAbs (x /- (ComB I)) = K :- I
bracketAbs (x /- (ComB (Var y))) = if (Var x) == (Var y) then I else K :- (Var y) 
bracketAbs (x /- (ComB (y :- z))) = if z == (Var x) && (not (x `elem` (freeVar (ComB y)))) then y else S :- (bracketAbs (x /- (ComB y))) :- (bracketAbs (x /- (ComB z))) 
bracketAbs (x /- y) = bracketAbs (x /- (ComB (bracketAbs y)))
-}
cnothing : Comb
cnothing = K

cjust : Comb
cjust = S :- (K :- (S :- (K :- K) :- (S :- I))) :- K


{- finEq : x = y -> Fin x = Fin y
finEq prf = rewrite prf in Refl

right_s_exchangeable_plus : (n : Nat) -> (m : Nat) -> plus n (S m) = S (plus n m)
right_s_exchangeable_plus Z m = Refl
right_s_exchangeable_plus (S k) m = rewrite right_s_exchangeable_plus k m in Refl

left_s_exch_plus : (n : Nat) -> (m : Nat) -> S (plus n m) = plus (S n) m
left_s_exch_plus Z m = Refl
left_s_exch_plus (S k) m = Refl

addFin : {n, m : Nat} -> Fin n -> Fin m -> Fin ((n + m))
addFin {n = S k} {m} FZ y = rewrite left_s_exch_plus k m in  weakenN k (FS y)
addFin (FS x) y = FS (addFin x y)

offset : (n : Nat) -> Fin m -> Fin (m+n)
offset Z x = weakenN Z x
offset (S k) x {m} = rewrite plusCommutative m (S k) in (id $ addFin (last {n = k}) x )



firstPart : {n : Nat} -> {m : Nat} -> {a : x `Has` n} -> {b : y `Has` m} -> Either x y -> Fin (n + m)
firstPart {n} {m} {a = (MkCnt (MkIso to from toFrom fromTo))} {b = (MkCnt (MkIso f g z w))} (Left l) = 
  (weakenN (m) (to l))
firstPart {n = S n} {m} {a = (MkCnt (MkIso to from toFrom fromTo))} {b = (MkCnt (MkIso f g z w))} (Right r) =
  (addFin (last {n = n}) (f r) )
firstPart {n = Z} {m = m} {x = x} {a = (MkCnt (MkIso to from toFrom fromTo))} {b = (MkCnt (MkIso f g z w))} (Left l) =
  weakenN m $ to l
firstPart {n = Z} {m = m} {a = (MkCnt (MkIso to from toFrom fromTo))} {b = (MkCnt (MkIso f g z w))} (Right r) =
  f r
secondPart : {a : x `Has` n} -> {b : y `Has` m} -> Fin (plus n m) -> Either x y
--secondPart {a} {b} x = ?secondPart_rhs

prfs1 : (y : Fin (plus n m)) -> firstPart (secondPart y) = y
prfs1 y = ?prfs1_rhs

prfs2 : (y : Either a b) -> secondPart (firstPart y) = y
prfs2 y = ?prfs2_rhs

eitherSum : {n : Nat} -> {m : Nat} -> (a : x `Has` n) -> (b : y `Has` m) -> Either x y `Has` (n + m)
eitherSum {n} {m} a@(MkCnt (MkIso to from toFrom fromTo)) b@(MkCnt (MkIso f g y z)) =
 (MkCnt (MkIso (firstPart {a = a} {b = b}) (secondPart {a = a} {b = b}) (prfs1 {n = n} {m = m}) prfs2))
 -}

